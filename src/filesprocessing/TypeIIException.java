package filesprocessing;

/**
 * Exception that may occur in the data processing level. Most of the times it will be fatal to the success
 * of the program's purpose, and hence the program will stop if encountered such exception.
 */
public class TypeIIException extends Exception {
    private static final long serialVersionUID = 1L;

    public TypeIIException(String message) {
        super(message);
    }

    public TypeIIException() {
        this("Type II exception");
    }
}
