package filesprocessing.sections.filters;


import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * This class represents a non specific NOT filter. It only get specific when creating a new instance of
 * this class, and then must hand the constructor a specific filter strategy. In that point the object has
 * a specific NOT strategy. The strategy is to do the exact opposite from the given filter - If the given
 * filter would have pass a certain file, the not strategy will fail it. And if the given filter would
 * have fail a certain file, the not strategy will pass it .
 */
class Not implements Filterable {

    private Filterable filterStrategy;

    /**
     * a constructor which creates a specific NOT filter strategy (because it gets a specific filter
     * strategy).
     * @param filterStrategy a filter in which this instance will do its exact NOT filtering strategy.
     */
    Not(Filterable filterStrategy) {
        this.filterStrategy = filterStrategy;
    }


    @Override
    public List<File> filter(List<File> files) {
        Set<File> filteredByOriginalStrategyFiles = new HashSet<>(filterStrategy.filter(files)); /* This
        set holds all the files that has been filtered by the original filter strategy */


        List<File> filteredFiles = new ArrayList<>();

        for (File file : files) {
            if (!filteredByOriginalStrategyFiles.contains(file)) { /* If a file did not pass the original
            strategy, then we want to pass it (Otherwise it passed the original strategy, then we don't
            want to pass it). */
                filteredFiles.add(file);
            }
        }

        return filteredFiles;
    }
}
