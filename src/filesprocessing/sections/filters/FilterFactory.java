package filesprocessing.sections.filters;


import java.util.HashSet;
import java.util.Set;

/**
 * This class purpose is to generate filters by given data.
 */
public class FilterFactory {

    /* Possible data messages of all filters */
    private static final String GREATER = "greater_than";
    private static final String SMALLER = "smaller_than";
    private static final Set<String> oneParameterSizeFilterSet;
    static {
        oneParameterSizeFilterSet = new HashSet<>();
        oneParameterSizeFilterSet.add(GREATER);
        oneParameterSizeFilterSet.add(SMALLER);
    }

    private static final String BETWEEN = "between";
    private static final Set<String> twoParameterSizeFilterSet;
    static {
        twoParameterSizeFilterSet = new HashSet<>();
        twoParameterSizeFilterSet.add(BETWEEN);
    }


    private static final String FILE = "file";
    private static final String CONTAINS = "contains";
    private static final String PREFIX = "prefix";
    private static final String SUFFIX = "suffix";

    private static final String WRITABLE = "writable";
    private static final String EXECUTABLE = "executable";
    private static final String HIDDEN = "hidden";
    private static final String YES = "YES";
    private static final String NO = "NO";

    private static final String ALL = "all";

    private static final String NOT = "NOT";

    private static final int FILTER_NAME = 0;
    private static final int PARAMETER = 1;
    private static final int PARAMETER_2 = 2;

    private static final int NO_PARAMETER_COMMAND_LENGTH = 1;
    private static final int ONE_PARAMETER_COMMAND_LENGTH = 2;
    private static final int TWO_PARAMETER_COMMAND_LENGTH = 3;

    private static final int BtoKB = 1024;

    /**
     * This method takes a length (which represent number of arguments) and a required command length
     * (number of arguments) and returns true if it fits. if there is a 'not' suffix in the command, it
     * calculates it in accordance.
     * @param length number of actual given parameters
     * @param commandLength required number of parameters
     * @param notSuffix true if there is a 'not' suffix in this command, false otherwise.
     * @return true if the actual number of parameters suits the required number of parameters, false
     * otherwise.
     */
    private static boolean lengthFit(int length, int commandLength, boolean notSuffix) {
        return (length == commandLength && !notSuffix) || (length == commandLength+1 && notSuffix);
    }


    /**
     * Generates a filter that has a double parameter.
     * @param commands instructions to create filter in the agreed format
     * @param notSuffix true if this command has a not suffix, false otherwise
     * @return a filter object with the requested strategy and parameter.
     * @throws FilterException if there is a problem with parsing the parameter into a double, or if the
     * filter name does not match any existing filter or if the command has too many values - the method
     * throws a FilterException.
     */
    private static Filterable oneParameterDoubleFormat(String[] commands, boolean notSuffix)
            throws FilterException {
        if (lengthFit(commands.length, ONE_PARAMETER_COMMAND_LENGTH, notSuffix)) {
            Double parameter;
            try {
                parameter = Double.valueOf(commands[PARAMETER]) * BtoKB;
                if (parameter < 0) {
                    throw new NegativeValueException();
                }
            } catch (NumberFormatException e) {
                throw new NonNumberValueException();
            }
            switch (commands[FILTER_NAME]) {
                case GREATER:
                    return new OneParameterFilter<Double>(FilterFunctionFactory.greaterThan(), parameter);
                case SMALLER:
                    return new OneParameterFilter<Double>(FilterFunctionFactory.smallerThan(), parameter);
                default:
                    throw new FilterNameException();
            }
        } else {
            throw new FilterException("Wrong usage of a size filter.");
        }
    }

    /**
     * Generates a filter that has 2 double parameters.
     * @param commands instructions to create filter in the agreed format
     * @param notSuffix true if this command has a not suffix, false otherwise
     * @return a filter object with the requested strategy and parameters.
     * @throws FilterException if there is a problem with parsing the parameters into a doubles, or if the
     * filter name does not match any existing filter or if the command has too many values - the method
     * throws a FilterException.
     */
    private static Filterable twoParameterDoubleFormat (String[] commands, boolean notSuffix)
            throws FilterException {
        if (lengthFit(commands.length, TWO_PARAMETER_COMMAND_LENGTH, notSuffix)) {
            Double parameter1;
            Double parameter2;
            try {
                parameter1 = Double.valueOf(commands[PARAMETER]) * BtoKB;
                parameter2 = Double.valueOf(commands[PARAMETER_2]) * BtoKB;
                if (parameter1 < 0 || parameter2 < 0) {
                    throw new NegativeValueException();
                }
                if (parameter1 > parameter2) {
                    throw new ValuesOrderException();
                }
            } catch (NumberFormatException e) {
                throw new NonNumberValueException();
            }
            switch (commands[FILTER_NAME]) {
                case BETWEEN:
                    return new TwoParameterFilter<Double, Double>(FilterFunctionFactory.between(),
                            parameter1, parameter2);
                default:
                    throw new FilterNameException();
            }
        } else {
            throw new FilterException("Wrong usage of a size filter.");
        }
    }

    /**
     * Generates a filter that has some amount of double parameters.
     * @param commands instructions to create filter in the agreed format
     * @param notSuffix true if this command has a not suffix, false otherwise
     * @return a filter object with the requested strategy and parameters.
     * @throws FilterException if there is a problem with creating the filter object it throws a
     * FilterException.
     */
    private static Filterable doubleFormat(String[] commands, boolean notSuffix) throws FilterException {
        if (oneParameterSizeFilterSet.contains(commands[FILTER_NAME])) {
            return oneParameterDoubleFormat(commands, notSuffix);
        } else if (twoParameterSizeFilterSet.contains(commands[FILTER_NAME])) {
            return twoParameterDoubleFormat(commands, notSuffix);
        }
        throw new FilterException();
    }

    /**
     * Generates a filter that has a String parameter.
     * @param commands instructions to create filter in the agreed format
     * @param notSuffix true if this command has a not suffix, false otherwise
     * @return a filter object with the requested strategy and parameter.
     * @throws FilterException if the filter name does not match any existing filter or if the command has
     * too many values - the method throws a FilterException.
     */
    private static Filterable stringFormat(String[] commands, boolean notSuffix) throws FilterException {
        if (lengthFit(commands.length, ONE_PARAMETER_COMMAND_LENGTH, notSuffix)) {
            switch (commands[FILTER_NAME]) {
                case FILE:
                    return new OneParameterFilter<String>(FilterFunctionFactory.fileName(),
                            commands[PARAMETER]);
                case CONTAINS:
                    return new OneParameterFilter<String>(FilterFunctionFactory.contains(),
                            commands[PARAMETER]);
                case PREFIX:
                    return new OneParameterFilter<String>(FilterFunctionFactory.prefix(),
                            commands[PARAMETER]);
                case SUFFIX:
                    return new OneParameterFilter<String>(FilterFunctionFactory.suffix(),
                            commands[PARAMETER]);
                default:
                    throw new FilterNameException();
            }
        }
        throw new FilterException("Wrong usage of a name filter.");
    }

    /**
     * Generates a filter that has a YES/NO parameter.
     * @param commands instructions to create filter in the agreed format
     * @param notSuffix true if this command has a not suffix, false otherwise
     * @return a filter object with the requested strategy and parameter.
     * @throws FilterException if there is a problem with the parameter (not YES or NO), or if the
     * filter name does not match any existing filter or if the command has too many values - the method
     * throws a FilterException.
     */
    private static Filterable boolFormat(String[] commands, boolean notSuffix) throws FilterException {
        if (lengthFit(commands.length, ONE_PARAMETER_COMMAND_LENGTH, notSuffix)) {
            boolean paramBool;
            switch (commands[PARAMETER]) {
                case YES:
                    paramBool = true;
                    break;
                case NO:
                    paramBool = false;
                    break;
                default:
                    switch (commands[FILTER_NAME]) {
                        case WRITABLE:
                            throw new PropertiesException("Wrong parameter for writable filter");
                        case EXECUTABLE:
                            throw new PropertiesException("Wrong parameter for executable filter");
                        case HIDDEN:
                            throw new PropertiesException("Wrong parameter for hidden filter");
                        default:
                            throw new PropertiesException();
                    }
            }

            switch (commands[FILTER_NAME]) {
                case WRITABLE:
                    return new OneParameterFilter<Boolean>(FilterFunctionFactory.writable(), paramBool);
                case EXECUTABLE:
                    return new OneParameterFilter<Boolean>(FilterFunctionFactory.executable(), paramBool);
                case HIDDEN:
                    return new OneParameterFilter<Boolean>(FilterFunctionFactory.hidden(), paramBool);
                default:
                    throw new FilterNameException();
            }
        }
        throw new FilterException("Wrong usage of a properties filter.");
    }

    /**
     * Generates a filter that has no parameters.
     * @param commands instructions to create filter in the agreed format
     * @param notSuffix true if this command has a not suffix, false otherwise
     * @return a filter object with the requested strategy.
     * @throws FilterException if the filter name does not match any existing filter or if the command has
     * too many values - the method throws a FilterException.
     */
    private static Filterable nonParameterFormat(String[] commands, boolean notSuffix)
            throws FilterException {
        if (lengthFit(commands.length, NO_PARAMETER_COMMAND_LENGTH, notSuffix)) {
            switch (commands[FILTER_NAME]) {
                case ALL:
                    return new NoParameterFilter(FilterFunctionFactory.all());
                default:
                    throw new FilterNameException();
            }
        }
        throw new FilterException("Wrong usage of no parameters filter.");
    }


    /**
     * This method gets a data that represents a filter request (in the accepted format), and returns a
     * suitable filterable object.
     * @param command represents the required filter in the agreed format.
     * @return a filterable object which suits the given command
     * @throws FilterException if it occurs a problem while analyzing the given command to a legal order it
     * will throw an OrderException.
     */
    public static Filterable createFilter(String command) throws FilterException {

        String[] commandSplit = command.split("#");

        if (commandSplit.length > 0) {
            boolean notSuffix = commandSplit[commandSplit.length-1].equals(NOT); /* last parameter equals
            NOT */

            Filterable filter;

            switch (commandSplit[FILTER_NAME]) {
                case GREATER:
                case SMALLER:
                case BETWEEN:
                    filter = doubleFormat(commandSplit, notSuffix);
                    break;
                case FILE:
                case CONTAINS:
                case PREFIX:
                case SUFFIX:
                    filter = stringFormat(commandSplit, notSuffix);
                    break;
                case WRITABLE:
                case EXECUTABLE:
                case HIDDEN:
                    filter = boolFormat(commandSplit, notSuffix);
                    break;
                case ALL:
                    filter = nonParameterFormat(commandSplit, notSuffix);
                    break;
                default:
                    throw new FilterNameException();
            }

            if (filter != null) {
                if (notSuffix) {
                    return new Not(filter);
                }
                return filter;
            } else {
                throw new FilterNameException();
            }
        } else {  // There is a wrong amount of parameters
            throw new FilterException("Wrong usage of filter");
        }
    }

    /**
     * This method returns the default filter of all existing orders.
     * @return a filterable object, which is the default order.
     */
    public static Filterable createDefaultFilter() {
        return new NoParameterFilter(FilterFunctionFactory.all());
    }
}
