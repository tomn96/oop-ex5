package filesprocessing.sections.filters;

/**
 * This Exception is an exception that occurs when a given parameter is negative.
 */
class NegativeValueException extends FilterException {
    private static final long serialVersionUID = 1L;

    NegativeValueException(String message) {
        super(message);
    }

    NegativeValueException() {
        this("Negative value was entered as a parameter to a size filter");
    }
}
