package filesprocessing.sections.filters;


import function.ThreePredicate;

import java.io.File;
import java.util.function.BiPredicate;
import java.util.function.Predicate;


/**
 * This class defines static methods that returns predicates for specific filters.
 */
class FilterFunctionFactory {

    /**
     * @return predicate to filter by if a file's size is greater than a certain value.
     */
    static BiPredicate<File, Double> greaterThan() {
        return (file, aDouble) -> file.length() > aDouble;
    }

    /**
     * @return predicate to filter by if a file's size is smaller than a certain value.
     */
    static BiPredicate<File, Double> smallerThan() {
        return (file, aDouble) -> file.length() < aDouble;
    }

    /**
     * @return predicate to filter by if a file's size is between two values.
     */
    static ThreePredicate<File, Double, Double> between() {
        return (file, aDouble, aDouble2) -> (aDouble <= file.length()) && (file.length() <= aDouble2);
    }

    /**
     * @return predicate to filter by file name
     */
    static BiPredicate<File, String> fileName() {
        return (file, aString) -> file.getName().equals(aString);
    }

    /**
     * @return predicate to filter by if a string contains another sub string.
     */
    static BiPredicate<File, String> contains() {
        return (file, aString) -> file.getName().contains(aString);
    }

    /**
     * @return predicate to filter by if a string starts with another string.
     */
    static BiPredicate<File, String> prefix() {
        return (file, aString) -> file.getName().startsWith(aString);
    }

    /**
     * @return predicate to filter by if a string ends with another string.
     */
    static BiPredicate<File, String> suffix() {
        return (file, aString) -> file.getName().endsWith(aString);
    }

    /**
     * @return predicate to filter by if file is writable.
     */
    static BiPredicate<File, Boolean> writable() {
        return (file, aBoolean) -> file.canWrite() == aBoolean;
    }

    /**
     * @return predicate to filter by if file is executable.
     */
    static BiPredicate<File, Boolean> executable() {
        return (file, aBoolean) -> file.canExecute() == aBoolean;
    }

    /**
     * @return predicate to filter by if file is hidden.
     */
    static BiPredicate<File, Boolean> hidden() {
        return (file, aBoolean) -> file.isHidden() == aBoolean;
    }

    /**
     * @return predicate for a filter that pass any file.
     */
    static Predicate<File> all() {
        return file -> true;
    }
}
