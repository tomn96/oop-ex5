package filesprocessing.sections.filters;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * This class represents a filter that has no parameters as its input. Its strategy is defined by the given
 * tester function.
 */
class NoParameterFilter implements Filterable {

    /* A tester function to use while filtering - this is the element that gives to a NoParameterFilter
    instance a specific filter strategy */
    private Predicate<File> testerFunction;

    /**
     * Constructor that initialize the testerFunction.
     * @param testerFunction a predicate that defines the filter's strategy of filtering.
     */
    NoParameterFilter(Predicate<File> testerFunction) {
        this.testerFunction = testerFunction;
    }


    @Override
    public List<File> filter(List<File> files) {
        List<File> filteredFiles = new ArrayList<>();

        for (File file : files) {
            if (testerFunction.test(file)) { /* uses the tester function to see if
                the file should pass the filter or not */
                filteredFiles.add(file);
            }
        }
        return filteredFiles;
    }
}
