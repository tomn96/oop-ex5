package filesprocessing.sections.filters;

/**
 * This Exception is a general exception that may occur while dealing with filters.
 */
public class FilterException extends Exception {
    private static final long serialVersionUID = 1L;

    public FilterException(String message) {
        super(message);
    }

    public FilterException() {
        this("Filter general exception");
    }
}
