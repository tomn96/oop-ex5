package filesprocessing.sections.filters;

/**
 * This Exception is an exception that occurs when there are 2 given parameters, and the first one is
 * greater than the second one.
 */
class ValuesOrderException extends FilterException {

    private static final long serialVersionUID = 1L;

    ValuesOrderException(String message) {
        super(message);
    }

    ValuesOrderException() {
        this("Values were entered in an invalid order. First value must be smaller than the second value.");
    }
}
