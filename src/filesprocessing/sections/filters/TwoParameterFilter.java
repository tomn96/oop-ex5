package filesprocessing.sections.filters;


import function.ThreePredicate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * This class represents a filter that has 2 parameters as its input. Its strategy is defined by the given
 * tester function.
 *
 * @param <T> The type of the first parameter of the filter
 * @param <U> The type of the second parameter of the filter
 */
class TwoParameterFilter<T, U> implements Filterable {

    /* A tester function to use while filtering - this is the element that gives to a NoParameterFilter
    instance a specific filter strategy */
    private ThreePredicate<File, T, U> testerFunction;

    private T parameter1;
    private U parameter2;

    /**
     * Constructor that initialize the testerFunction and the parameters.
     * @param testerFunction a predicate that defines the filter's strategy of filtering.
     * @param parameter1 the filter's first parameter which the filter strategy filters by.
     * @param parameter2 the filter's second parameter which the filter strategy filters by.
     */
    TwoParameterFilter(ThreePredicate<File, T, U> testerFunction, T parameter1, U parameter2) {
        this.testerFunction = testerFunction;
        this.parameter1 = parameter1;
        this.parameter2 = parameter2;
    }


    @Override
    public List<File> filter(List<File> files) {
        List<File> filteredFiles = new ArrayList<>();

        for (File file : files) {
            if (testerFunction.test(file, parameter1, parameter2)) { /* uses the tester function to
                see if the file should pass the filter or not */
                filteredFiles.add(file);
            }
        }
        return filteredFiles;
    }
}
