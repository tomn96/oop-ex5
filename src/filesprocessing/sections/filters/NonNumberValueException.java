package filesprocessing.sections.filters;

/**
 * This Exception is an exception that occurs when a given parameter suppose to be a number but it is not.
 */
class NonNumberValueException extends FilterException {
    private static final long serialVersionUID = 1L;

    NonNumberValueException(String message) {
        super(message);
    }

    NonNumberValueException() {
        this("A not numeric value was entered as a parameter to a size filter");
    }
}
