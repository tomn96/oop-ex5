package filesprocessing.sections.filters;

/**
 * This exception occurs when there is data that contains a name of a filter (allegedly), but actually the
 * name is wrong.
 */
class FilterNameException extends FilterException {
    private static final long serialVersionUID = 1L;

    FilterNameException(String message) {
        super(message);
    }

    FilterNameException() {
        this("A non existing filter name was entered");
    }
}
