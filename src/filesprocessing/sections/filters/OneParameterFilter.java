package filesprocessing.sections.filters;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

/**
 * This class represents a filter that has 1 parameter as its input. Its strategy is defined by the given
 * tester function.
 *
 * @param <T> The type of the filter's parameter
 */
class OneParameterFilter<T> implements Filterable {

    /* A tester function to use while filtering - this is the element that gives to a NoParameterFilter
    instance a specific filter strategy */
    private BiPredicate<File, T> testerFunction;

    private T parameter;

    /**
     * Constructor that initialize the testerFunction and the parameter.
     * @param testerFunction a predicate that defines the filter's strategy of filtering.
     * @param parameter the filter's parameter which the filter strategy filters by.
     */
    OneParameterFilter(BiPredicate<File, T> testerFunction, T parameter) {
        this.testerFunction = testerFunction;
        this.parameter = parameter;
    }


    @Override
    public List<File> filter(List<File> files) {
        List<File> filteredFiles = new ArrayList<>();

        for (File file : files) {
            if (testerFunction.test(file, parameter)) { /* uses the tester function to see if
                the file should pass the filter or not */
                filteredFiles.add(file);
            }
        }
        return filteredFiles;
    }
}
