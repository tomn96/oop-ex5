package filesprocessing.sections.filters;

/**
 * This Exception is an exception that may occur while dealing with properties filters.
 */
class PropertiesException extends FilterException {
    private static final long serialVersionUID = 1L;

    PropertiesException(String message) {
        super(message);
    }

    PropertiesException() {
        this("Wrong parameter for a property filter");
    }
}
