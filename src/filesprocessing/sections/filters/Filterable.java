package filesprocessing.sections.filters;

import java.io.File;
import java.util.List;

/**
 * This interface defines the method that all filters should (and must) have, with the aim of using this
 * interface while interacting with any specific order.
 */
public interface Filterable {
    /**
     * This method takes a list of files and returns a list of the files that passed the filter.
     * THIS METHOD IS NOT ALLOWED TO CHANGE THE GIVEN LIST!
     * @param files The list of files to filter.
     * @return A list of the filtered files.
     */
    List<File> filter(List<File> files);
}
