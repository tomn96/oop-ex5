package filesprocessing.sections.orders;


import java.io.File;
import java.util.List;

/**
 * This class is singleton that implements an order strategy. It orders files alphabetically by their
 * names.
 */
class AbsOrder implements Orderable {

    /* This is the specific instance of the class (singleton) */
    private static AbsOrder absOrder = new AbsOrder();

    /**
     * This method returns an instance of the class (singleton).
     * @return an Orderable object that has this specific class's strategy
     */
    static AbsOrder getInstance() {
        return absOrder;
    }

    @Override
    public List<File> order(List<File> files) {
        files.sort((o1, o2) -> o1.getName().compareTo(o2.getName())); /* orders the files by the class's
        strategy (orders alphabetically by name) */
        return files;
    }
}
