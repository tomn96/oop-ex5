package filesprocessing.sections.orders;


/**
 * This class purpose is to generate orders by given data.
 */
public class OrderFactory {

    /* Possible data messages of all orders */
    private static final String ABS = "abs";
    private static final String TYPE = "type";
    private static final String SIZE = "size";

    private static final String REVERSE = "REVERSE";

    private static final int WITHOUT_REVERSE_SUFFIX_LENGTH = 1;
    private static final int WITH_REVERSE_SUFFIX_LENGTH = 2;

    private static final int ORDER_NAME = 0;
    private static final int REVERSE_SUFFIX = 1;

    /**
     * This method gets a data that represents an order request (in the accepted format), and returns a
     * suitable order.
     * @param command represents the required order in the agreed format.
     * @return an order which suits the given command
     * @throws OrderException if it occurs a problem while analyzing the given command to a legal order it
     * will throw an OrderException.
     */
    public static Orderable createOrder(String command) throws OrderException {
        String[] commandSplit = command.split("#");

        Orderable order;

        if (commandSplit.length == WITHOUT_REVERSE_SUFFIX_LENGTH ||
                commandSplit.length == WITH_REVERSE_SUFFIX_LENGTH) { /* There is a possible right amount
                of parameters */
            switch (commandSplit[ORDER_NAME]) {
                case ABS:
                    order = AbsOrder.getInstance();
                    break;
                case TYPE:
                    order = TypeOrder.getInstance();
                    break;
                case SIZE:
                    order = SizeOrder.getInstance();
                    break;
                default:
                    throw new OrderNameException();
            }

            if (order != null) { // order was initialized => the first argument was fine.
                if (commandSplit.length == WITH_REVERSE_SUFFIX_LENGTH) { // there are 2 args, not only 1.
                    if (commandSplit[REVERSE_SUFFIX].equals(REVERSE)) {
                        return new Reverse(order);
                    } else {
                        throw new OrderException("Wrong second parameter");
                    }
                }
                return order;
            } else { /* order was not initialized (order == null) => the first argument was none of the
            possible values. */
                throw new OrderNameException();
            }
        } else { // There is a wrong amount of parameters
            throw new OrderException("Wrong order usage - incorrect number of parameters.");
        }
    }

    /**
     * This method returns the default order of all existing orders.
     * @return an orderable object, which is the default order.
     */
    public static Orderable createDefaultOrder() {
        return AbsOrder.getInstance();
    }
}
