package filesprocessing.sections.orders;

/**
 * This Exception is a general exception that may occur while dealing with orders.
 */
public class OrderException extends Exception {
    private static final long serialVersionUID = 1L;

    public OrderException(String message) {
        super(message);
    }

    public OrderException() {
        this("Order general exception");
    }
}
