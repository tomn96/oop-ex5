package filesprocessing.sections.orders;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * This class represents a non specific reverse order. It only get specific when creating a new instance of
 * this class, and then must hand the constructor a specific order strategy. In that point the object has a
 * specific reverse strategy - the strategy is to do the exact same order as the order strategy (that was
 * given in the construction) would do, only reverse.
 */
class Reverse implements Orderable {

    private Orderable orderStrategy;

    /**
     * a constructor which creates a specific reverse order strategy (because it gets a specific order
     * strategy).
     * @param orderStrategy an order in which this instance will do its exact reverse order
     */
    Reverse(Orderable orderStrategy) {
        this.orderStrategy = orderStrategy;
    }

    @Override
    public List<File> order(List<File> files) {
        files = orderStrategy.order(files); // make the order as the order strategy given at construction
        Collections.reverse(files); // reverse the order
        return files;
    }
}
