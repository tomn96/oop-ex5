package filesprocessing.sections.orders;


import java.io.File;
import java.util.List;

/**
 * This class is singleton that implements an order strategy. It orders files alphabetically by their
 * type.
 */
class TypeOrder implements Orderable {

    /* This is the specific instance of the class (singleton) */
    private static TypeOrder typeOrder = new TypeOrder();

    /**
     * This method returns an instance of the class (singleton).
     * @return an Orderable object that has this specific class's strategy
     */
    static TypeOrder getInstance() {
        return typeOrder;
    }

    /**
     * This method takes a file name string and returns its type as a string. If a file has no type it
     * returns an empty string. If a file name starts with a period and does not contain another period, it
     * returns an empty string.
     * @param name the file name to give its type
     * @return the given file's type as described above.
     */
    private static String getType(String name) {
        String[] nameSplit = name.split("\\.");
        if (nameSplit.length == 1) { // The file name has no type suffix.
            return "";
        } else if (nameSplit.length == 2 && nameSplit[0].equals("")) { /* file name starts with a period
        and does not contain another period */
            return "";
        } else {
            return nameSplit[nameSplit.length-1];
        }
    }

    @Override
    public List<File> order(List<File> files) {

        files.sort((o1, o2) -> {
            String name1 = o1.getName();
            String name2 = o2.getName();

            String type1 = getType(name1);
            String type2 = getType(name2);

            int answer = type1.compareTo(type2);
            if (answer == 0) { // The two files has the same type. Then it orders them by name:
                return name1.compareTo(name2);
            }
            return answer;
        }); /* orders the files by the class's strategy (orders alphabetically by file's type - if two
        files has the same type, it orders them by name) */

        return files;
    }
}
