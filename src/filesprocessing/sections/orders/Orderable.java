package filesprocessing.sections.orders;

import java.io.File;
import java.util.List;

/**
 * This interface defines the method that all orders should (and must) have, with the aim of using this
 * interface while interacting with any specific order.
 */
public interface Orderable {
    /**
     * This method takes a list of files and returns a list of the same files but in order.
     * @param files The list of files to order.
     * @return A list of the ordered files.
     */
    List<File> order(List<File> files);
}
