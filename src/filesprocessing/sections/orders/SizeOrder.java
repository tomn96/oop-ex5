package filesprocessing.sections.orders;


import java.io.File;
import java.util.List;

/**
 * This class is singleton that implements an order strategy. It orders files by their sizes.
 */
class SizeOrder implements Orderable {

    /* This is the specific instance of the class (singleton) */
    private static SizeOrder ourInstance = new SizeOrder();

    /**
     * This method returns an instance of the class (singleton).
     * @return an Orderable object that has this specific class's strategy
     */
    static SizeOrder getInstance() {
        return ourInstance;
    }


    @Override
    public List<File> order(List<File> files) {

        files.sort((o1, o2) -> {
            long size1 = o1.length();
            long size2 = o2.length();

            if (size1 > size2) {
                return 1; // o1 > o2
            } else if (size1 < size2) {
                return -1; // o2 < o1
            } else {
                return o1.getName().compareTo(o2.getName());
            }
        }); /* orders the files by the class's strategy (orders by size) */

        return files;
    }
}
