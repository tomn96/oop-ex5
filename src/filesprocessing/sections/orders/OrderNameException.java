package filesprocessing.sections.orders;

/**
 * This exception occurs when there is data that contains a name of an order (allegedly), but actually the
 * name is wrong.
 */
class OrderNameException extends OrderException {
    private static final long serialVersionUID = 1L;

    OrderNameException(String message) {
        super(message);
    }

    OrderNameException() {
        this("A non existing order name was entered");
    }
}
