package filesprocessing.sections;


import filesprocessing.sections.filters.FilterException;
import filesprocessing.sections.filters.FilterFactory;
import filesprocessing.sections.filters.Filterable;
import filesprocessing.sections.orders.OrderException;
import filesprocessing.sections.orders.OrderFactory;
import filesprocessing.sections.orders.Orderable;

import java.io.File;
import java.util.List;

/**
 * This class represents a Section as it was described in the exercise. It has a filter strategy and an
 * order strategy, and it can print given files after they were filtered and ordered by the specific
 * strategies.
 */
public class Section implements Filterable, Orderable {

    private static final String WARNING_TAG_MESSAGE = "Warning in line ";

    /* STRATEGIES - filter and order strategies */
    private Filterable filterStrategy;
    private Orderable orderStrategy;

    /* Data to create the required strategies */
    private String filterCommands, orderCommands;
    private int filterLine, orderLine;

    /**
     * Constructor for the section object which initializes the required data to create the filter and
     * order strategies.
     * @param filterCommands filter command data
     * @param filterLine the line in file in which the filter command was in
     * @param orderCommands order command data
     * @param orderLine the line in file in which the order command was in
     */
    public Section(String filterCommands, int filterLine, String orderCommands, int orderLine) {
        this.filterCommands = filterCommands;
        this.orderCommands = orderCommands;
        this.filterLine = filterLine;
        this.orderLine = orderLine;
    }

    /**
     * Initializes the filter strategy. It prints the required (described in the exercise) warning if it
     * occurs with a problem while initializing the filter strategy. In this scenario it uses the default
     * filter as its strategy.
     */
    private void initializeFilter() {
        try {
            filterStrategy = FilterFactory.createFilter(filterCommands);
        } catch (FilterException e) {
            System.err.println(WARNING_TAG_MESSAGE + filterLine);
            filterStrategy = FilterFactory.createDefaultFilter();
        }
    }

    /**
     * Initializes the order strategy. It prints the required (described in the exercise) warning if it
     * occurs with a problem while initializing the order strategy. In this scenario it uses the default
     * order as its strategy.
     */
    private void initializeOrder() {
        try {
            orderStrategy = OrderFactory.createOrder(orderCommands);
        } catch (OrderException e) {
            System.err.println(WARNING_TAG_MESSAGE + orderLine);
            orderStrategy = OrderFactory.createDefaultOrder();
        }
    }

    /**
     * Initializes both filter and order strategies. if it occurs a problem while initializing on of them,
     * the required warning will be printed.
     */
    private void initialize() {
        initializeFilter();
        initializeOrder();
    }

    @Override
    public List<File> filter(List<File> files) {
        if (filterStrategy == null) {
            initializeFilter();
        }
        return filterStrategy.filter(files);
    }

    @Override
    public List<File> order(List<File> files) {
        if (orderStrategy == null) {
            initializeOrder();
        }
        return orderStrategy.order(files);
    }

    /**
     * This method filters and orders the given files by the section's specific strategies.
     * @param files the files list to filter and order
     * @return the filtered and ordered list of files
     */
    private List<File> operate(List<File> files) {
        initialize();
        files = filter(files);
        files = order(files);
        return files;
    }

    /**
     * This method takes the given files list and prints the filtered files (names) in the specific order.
     * @param files the files to filter and print in order
     */
    public void printSuitableFiles(List<File> files) {
        files = operate(files);
        for (File file : files) {
            System.out.println(file.getName());
        }
    }
}
