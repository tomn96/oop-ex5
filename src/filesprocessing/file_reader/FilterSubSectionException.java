package filesprocessing.file_reader;

/**
 * This exception occurs when a FILTER sub-section does not written as it suppose to be. For example, if
 * there is no FILTER sub-section at all, or if it is not written right.
 */
class FilterSubSectionException extends CommandFileException {
    private static final long serialVersionUID = 1L;

    FilterSubSectionException(String message) {
        super(message);
    }

    FilterSubSectionException() {
        this("Filter sub-section does not exist or not in the correct format");
    }
}
