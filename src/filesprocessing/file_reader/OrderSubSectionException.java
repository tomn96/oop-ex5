package filesprocessing.file_reader;

/**
 * This exception occurs when an ORDER sub-section does not written as it suppose to be. For example, if
 * there is no ORDER sub-section at all, or if it is not written right.
 */
class OrderSubSectionException extends CommandFileException {
    private static final long serialVersionUID = 1L;

    OrderSubSectionException(String message) {
        super(message);
    }

    OrderSubSectionException() {
        this("Order sub-section does not exist or not in the correct format");
    }
}
