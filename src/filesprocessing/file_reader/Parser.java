package filesprocessing.file_reader;


import filesprocessing.sections.Section;
import filesprocessing.sections.filters.FilterException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class purpose is to parse data according to the agreed format which is described in the exercise.
 */
public class Parser {

    /* CONSTANTS - FILTER/ORDER labels format */
    private static final String FILTER = "FILTER";
    private static final String ORDER = "ORDER";

    private static final String DEFAULT_ORDER = "abs";

    /**
     * This method parses the data from the scanner according to the agreed format, and returns suitable
     * sections for the given data.
     * @param scanner a scanner which the method gets the data through.
     * @return a list of sections that are decided by the data from the scanner
     * @throws CommandFileException if there is a problem with the data's format it will throw a
     * CommandFileException.
     */
    public static List<Section> readScanner(Scanner scanner) throws CommandFileException {
        List<Section> sections = new ArrayList<>(); // will be eventually the return value

        String filterType, orderType;

        int lineNumber = 0; // counter for the line number in the scanner's data
        int filterLine, orderLine;

        while (scanner.hasNext()) { // while there is more data to get from the scanner

            if (scanner.hasNext(FILTER)) { // the next sub-section is a FILTER
                scanner.next();
                lineNumber++;

                if (scanner.hasNext()) {
                    if (scanner.hasNext(ORDER)) { // there is an ORDER sub-section right away
                        throw new FilterSubSectionException(
                                "A filter name after the FILTER label is missing");
                    } else {
                        filterType = scanner.next(); // gets the data about the filter from the scanner
                        lineNumber++;
                    }
                } else { // The FILTER label was the last line of the data
                    throw new CommandFileException("A filter name and an Order sub-section are missing");
                }
                filterLine = lineNumber;
            } else { // a FILTER label is missing from the agreed format
                throw new FilterSubSectionException();
            }

            if (scanner.hasNext(ORDER)) { // the next sub-section is an ORDER
                scanner.next();
                lineNumber++;

                if (scanner.hasNext()) {
                    if (scanner.hasNext(FILTER)) { /* there is a FILTER sub-section right away so the
                    order type is considered default */
                        orderType = DEFAULT_ORDER;
                    } else {
                        orderType = scanner.next(); // gets the data about the order from the scanner
                        lineNumber++;
                    }
                } else { // The ORDER label was the last line of the data
                    orderType = DEFAULT_ORDER;
                }
                orderLine = lineNumber;
            } else { // an ORDER label is missing from the agreed format
                throw new OrderSubSectionException();
            }

            /* if everything was parsed fine, it creates a new section according to the parsed data, and
            adds it to the final sections list: */
            sections.add(new Section(filterType,filterLine, orderType, orderLine));
        }

        return sections;
    }
}
