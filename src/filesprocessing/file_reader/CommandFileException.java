package filesprocessing.file_reader;


import filesprocessing.TypeIIException;

/**
 * This exception occurs if the command file does not meet the standards. Therefor it can not provide data
 * for the program to run as it suppose to.
 */
class CommandFileException extends TypeIIException {
    private static final long serialVersionUID = 1L;

    CommandFileException(String message) {
        super(message);
    }

    CommandFileException() {
        this("Command file does not meet the standards");
    }
}
