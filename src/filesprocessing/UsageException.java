package filesprocessing;

/**
 * This exception occurs if the user command line's input usage is wrong and does not meet the
 * specified requirements (see usage).
 */
class UsageException extends TypeIIException {
    private static final long serialVersionUID = 1L;

    UsageException(String message) {
        super(message);
    }

    UsageException() {
        this("Wrong usage");
    }
}
