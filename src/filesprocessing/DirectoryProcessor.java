package filesprocessing;


import filesprocessing.file_reader.Parser;
import filesprocessing.sections.Section;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Main class that takes the user inputs (source directory pth and command file path, see usage), and
 * prints the suitable files.
 */
public class DirectoryProcessor {

    private static final int RIGHT_AMOUNT_OF_ARGS = 2;

    private static final int SOURCE_DIR = 0;
    private static final int COMMAND_FILE = 1;

    private static final String ERROR_TAG = "ERROR: ";
    private static final String IO_EXCEPTION_MESSAGE = "Problem with the command file.";

    /**
     * Checks if the numbers of user input arguments is suitable to the requirements.
     * @param args array of the user input
     * @throws TypeIIException If there is a wrong number of arguments it throws a new TypeIIException
     */
    private static void checkArgs(String[] args) throws TypeIIException {
        if (args.length != RIGHT_AMOUNT_OF_ARGS) {
            throw new UsageException();
        }
    }

    /**
     * Creates the sections described in the command file, which is path is given.
     * @param commandFilePath path for the section's command file
     * @return a list of the sections described in the command file
     * @throws IOException If there is a problem with the file described in the path, it throws an
     * IOException.
     * @throws TypeIIException if there was a problem parsing the command file it throws a
     * TypeIIException.
     */
    private static List<Section> getSections(String commandFilePath) throws IOException,
            TypeIIException {
        File commandFile = new File(commandFilePath);
        if (!commandFile.exists()) {
            throw new FileNotFoundException(); // IOException
        }
        Scanner scanner = new Scanner(commandFile);
        return Parser.readScanner(scanner);
    }

    /**
     * This method searches for all files which present directly in the given path.
     * @param sourceDirPath the path to search for files in
     * @return a list of all the files in the given path
     * @throws IOException If there is a problem with the file described in the path, it throws an
     * IOException.
     */
    private static List<File> getFiles(String sourceDirPath) throws IOException {
        File sourcedir = new File(sourceDirPath);
        if (!sourcedir.exists()) {
            throw new FileNotFoundException();
        }
        File[] filesArray = sourcedir.listFiles();

        List<File> files = new ArrayList<>();
        if (filesArray != null) {
            for (File file : filesArray) {
                if (file.isFile()) {
                    files.add(file);
                }
            }
        }
        return files;
    }

    /**
     * prints all the files in the list by each section's rules.
     * @param sections a list of sections to print according to each one of them.
     * @param files a list of all the files to filter and order by each section.
     */
    private static void printRequest(List<Section> sections, List<File> files) {
        for (Section section : sections) {
            section.printSuitableFiles(files);
        }
    }

    /**
     * The main method that runs the program as described in the exercise.
     * @param args an array of the use's inputs.
     */
    public static void main(String[] args) {

        try {
            checkArgs(args); // may produce a UsageException

            List<Section> sections = getSections(args[COMMAND_FILE]); /* may produce a CommandFileException
            or an IOException */

            List<File> files = getFiles(args[SOURCE_DIR]); // may produce an IOException

            printRequest(sections, files);

        // Deals with type 2 exceptions as requested in the exercise:
        } catch (TypeIIException e) {
            System.err.println(ERROR_TAG + e.getMessage());
        } catch (IOException e) {
            System.err.println(ERROR_TAG + IO_EXCEPTION_MESSAGE);
        }
    }
}
